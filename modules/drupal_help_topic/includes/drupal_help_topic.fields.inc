<?php
/**
 * @file
 * Contains module specific custom fields, and field definitions in code.
 */

/**
 * Add default background information type field to a Documentation Entity type.
 *
 * @param string $documentation_entity_type
 *   Help topic type.
 * @param string $description
 *   Field instance description.
 * @param string $label
 *   The label for the background information type instance.
 * @param null|int $weight
 *   Widget and display weight.
 *
 * @return array
 *   Background information type field instance.
 */
function drupal_help_topic_add_bg_info_type_field($documentation_entity_type, $description = '', $label = 'Type', $weight = NULL) {
  $vid = drupal_help_topic_create_bg_info_type_vocabulary();
  drupal_help_topic_create_default_bg_info_type_terms();

  $field = field_info_field(FIELD_NAME_BG_INFO_TYPE);
  $instance = field_info_instance(ENTITY_NAME_DOCUMENTATION_ENTITY, FIELD_NAME_BG_INFO_TYPE, $documentation_entity_type);

  if (empty($field)) {
    $field = array(
      'field_name' => FIELD_NAME_BG_INFO_TYPE,
      'type' => 'taxonomy_term_reference',
      'entity_types' => array(ENTITY_NAME_DOCUMENTATION_ENTITY),
      'locked' => TRUE,
      'translatable' => TRUE,
      'settings' => array(
        'allowed_values' => array(
          0 => array(
            'vocabulary' => 'background_information_type',
            'parent' => 0,
          ),
        ),
        'no_ui' => TRUE,
      ),
    );
    $field = field_create_field($field);
  }

  if (empty($instance)) {
    $instance = array(
      'field_name' => FIELD_NAME_BG_INFO_TYPE,
      'entity_type' => ENTITY_NAME_DOCUMENTATION_ENTITY,
      'label' => $label,
      'description' => $description,
      'bundle' => $documentation_entity_type,
      'required' => TRUE,
      'widget' => array(
        'type' => 'options_select',
      ),
      'settings' => array(
        'no_ui' => TRUE,
      ),
      'display' => array(
        'default' => array(
          'type' => 'taxonomy_term_reference_plain',
        ),
      ),
    );
    if ($weight !== NULL || is_numeric($weight)) {
      $instance['widget']['weight'] = $weight;
      $instance['display']['default']['weight'] = $weight;
    }
    $instance = field_create_instance($instance);
  }
  return $instance;
}

/**
 * Create 'background_information_type' vocabulary.
 *
 * @return int
 *   Vocabulary identifier.
 */
function drupal_help_topic_create_bg_info_type_vocabulary() {
  $vocabulary = taxonomy_vocabulary_machine_name_load('background_information_type');
  if (empty($vocabulary)) {
    $vocabulary = (object) array(
      'name' => 'Background information type',
      'machine_name' => 'background_information_type',
      'description' => t('Background information types.'),
      'help' => t('Enter a comma-separated list of background information types.'),
    );
    taxonomy_vocabulary_save($vocabulary);
    $vocabulary = taxonomy_vocabulary_machine_name_load('background_information_type');
  }

  return $vocabulary->vid;
}

/**
 * Create default terms for 'background_information_type' vocabulary.
 */
function drupal_help_topic_create_default_bg_info_type_terms() {
  $terms = array(
    'Concept',
    'Reference material',
    'Module overview',
    'Theme overview',
    'Distribution overview',
  );
  $vocabulary = taxonomy_vocabulary_machine_name_load('background_information_type');

  foreach ($terms as $term_name) {
    $term = taxonomy_get_term_by_name($term_name, 'background_information_type');
    if (empty($term)) {
      $term = (object) array(
        'vid' => $vocabulary->vid,
        'name' => $term_name,
      );
      taxonomy_term_save($term);
    }
  }
}
