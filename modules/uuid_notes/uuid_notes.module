<?php
/**
 * @file
 * Provides a field which can store a UUID and arbitrary notes.
 */

/**
 * Implements hook_menu().
 */
function uuid_notes_menu() {
  $items = array();

  $items['uuid_notes/autocomplete/%/%'] = array(
    'title' => 'Autocomplete UUID',
    'page callback' => 'uuid_notes_autocomplete',
    'page arguments' => array(2, 3),
    'access arguments' => array(PERMISSION_VIEW_DOCUMENTATION_ENTITY),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_field_info().
 */
function uuid_notes_field_info() {
  return array(
    'uuid_notes' => array(
      'label' => t('UUID and notes'),
      'description' => t(''),
      'default_widget' => 'uuid_notes_default_widget',
      'default_formatter' => 'uuid_notes_default_formatter',
      'instance_settings' => array(
        'uuid_entity_type' => 'node',
        'uuid_required' => 0,
        'notes_required' => 0,
      ),
      'no_ui' => TRUE,
    ),
  );
}

/**
 * Implements hook_field_formatter_info().
 *
 * @see uuid_notes_field_formatter_view()
 */
function uuid_notes_field_formatter_info() {
  return array(
    // This formatter just displays a link and attached notes.
    'uuid_notes_default_formatter' => array(
      'label' => t('Link and notes'),
      'field types' => array('uuid_notes'),
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 *
 * @see uuid_notes_field_widget_form()
 */
function uuid_notes_field_widget_info() {
  return array(
    'uuid_notes_default_widget' => array(
      'label' => t('UUID and notes'),
      'field types' => array('uuid_notes'),
    ),
  );
}

/**
 * Implements hook_field_instance_settings_form().
 */
function uuid_notes_field_instance_settings_form($field, $instance) {
  $settings = $instance['settings'];
  $form = array();
  $options = array();

  foreach (entity_get_info() as $entity_type => $info) {
    if (isset($info['uuid']) && $info['uuid']) {
      $options[$entity_type] = $info['label'];
    }
  }

  $form['uuid_entity_type'] = array(
    '#type' => 'select',
    '#title' => t("UUID entity type"),
    '#options' => $options,
    '#default_value' => $settings['uuid_entity_type'],
    '#required' => TRUE,
    '#multiple' => FALSE,
  );

  $form['uuid_required'] = array(
    '#type' => 'checkbox',
    '#title' => t("UUID is required."),
    '#default_value' => $settings['uuid_required'],
  );

  $form['notes_required'] = array(
    '#type' => 'checkbox',
    '#title' => t("Notes are required."),
    '#default_value' => $settings['notes_required'],
  );

  return $form;
}

/**
 * Implements hook_field_validate().
 *
 * @see uuid_notes_field_widget_error()
 */
function uuid_notes_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  $settings = $instance['settings'];

  foreach ($items as $delta => $item) {
    if (!empty($item['uuid'])) {
      if (!uuid_is_valid($item['uuid'])) {
        $errors[$field['field_name']][$langcode][$delta]['uuid'] = array(
          'error' => 'uuid_notes_uuid_invalid_format',
          'message' => t('UUID must be in valid format.'),
        );
      }

      if ($settings['notes_required'] && empty($item['notes'])) {
        $errors[$field['field_name']][$langcode][$delta]['notes'] = array(
          'error' => 'uuid_notes_notes_are_required',
          'message' => t('UUID is filled, Notes cannot be empty.'),
        );
      }
    }

    if (!empty($item['notes']) && $settings['uuid_required'] && empty($item['uuid'])) {
      $errors[$field['field_name']][$langcode][$delta]['uuid'] = array(
        'error' => 'uuid_notes_uuid_is_required',
        'message' => t('Notes are filled, UUID cannot be empty.'),
      );
    }
  }
}

/**
 * Implements hook_field_widget_error().
 *
 * @see uuid_notes_field_validate()
 * @see form_error()
 */
function uuid_notes_field_widget_error($element, $error, $form, &$form_state) {
  switch ($error['error']) {
    case 'uuid_notes_uuid_invalid_format':
    case 'uuid_notes_uuid_is_required':
      form_error($element['uuid'], $error['message']);
      break;
    case 'uuid_notes_notes_are_required':
      form_error($element['notes'], $error['message']);
      break;
  }
}

/**
 * Implements hook_field_is_empty().
 */
function uuid_notes_field_is_empty($item, $field) {
  return empty($item['uuid']) && empty($item['notes']);
}

/**
 * Implements hook_field_formatter_view().
 *
 * @see uuid_notes_field_formatter_info()
 */
function uuid_notes_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $instance_settings = $instance['settings'];

  switch ($display['type']) {
    case 'uuid_notes_default_formatter':
      foreach ($items as $delta => $item) {
        if (!empty($item['uuid'])) {
          $entity = entity_uuid_load($instance_settings['uuid_entity_type'], array($item['uuid']));
          if (!empty($entity)) {
            $uri_label = uuid_notes_uuid_get_uri_label($item['uuid'], $instance_settings['uuid_entity_type']);
            $element[$delta]['uuid'] = array(
              '#type' => 'link',
              '#title' => $uri_label['label'],
              '#href' => $uri_label['uri']['path'],
            );
          }
          else {
            $element[$delta]['uuid'] = array(
              '#markup' => t('Topic could not be found locally. (@uuid)', array('@uuid' => $item['uuid'])),
            );
          }
        }
        $output = check_markup($item['notes'], $item['format'], $langcode);
        $element[$delta]['notes'] = array(
          '#title' => t('Notes'),
          '#markup' => $output,
        );
      }
      break;
  }

  return $element;
}

/**
 * Implements hook_field_widget_form().
 */
function uuid_notes_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $settings = $instance['settings'];
  $token = drupal_get_token('uuid_notes_autocomplete');

  switch ($instance['widget']['type']) {
    case 'uuid_notes_default_widget':
      $element['uuid'] = array(
        '#type' => 'textfield',
        '#title' => t('UUID'),
        '#default_value' => isset($items[$delta]['uuid']) ? $items[$delta]['uuid'] : NULL,
        '#autocomplete_path' => "uuid_notes/autocomplete/{$token}/{$settings['uuid_entity_type']}",
      );

      if ($settings['uuid_required']) {
        $element['uuid']['#description'] = t('If Notes are filled, the UUID is required as well.');
      }

      $element_info = element_info('text_format');
      $process = array_merge($element_info['#process'], array('uuid_notes_field_widget_process'));
      $element['notes'] = array(
        '#type' => 'text_format',
        '#title' => t('Notes'),
        '#default_value' => isset($items[$delta]['notes']) ? $items[$delta]['notes'] : '',
        '#format' => isset($items[$delta]['format']) ? $items[$delta]['format'] : NULL,
        '#process' => $process,
      );

      if ($settings['notes_required']) {
        $element['notes']['#description'] = t('If UUID is filled, Notes are required as well.');
      }

      break;
  }

  return $element;
}

/**
 * Additional process callback for the text_format element.
 *
 * @see uuid_notes_field_widget_form()
 * @see uuid_notes_field_widget_validate()
 */
function uuid_notes_field_widget_process($element) {
  $element['#element_validate'][] = 'uuid_notes_field_widget_validate';
  return $element;
}

/**
 * Element validate callback.
 *
 * For changing the structure of input values of the text_format element in
 * $form_state['values'] so that they map to the columns of the field schema.
 *
 * @see uuid_notes_field_widget_form()
 * @see uuid_notes_field_schema()
 */
function uuid_notes_field_widget_validate($element, &$form_state, $form) {
  $parents = $element['#parents'];

  $original_value = drupal_array_get_nested_value($form_state['values'], $parents);

  $notes_element_parents = $parents;
  array_pop($notes_element_parents);
  $notes_element_parents[] = 'notes';
  $value_element = array(
    '#parents' => $notes_element_parents,
  );
  form_set_value($value_element, $original_value['value'], $form_state);

  $format_element_parents = $parents;
  array_pop($format_element_parents);
  $format_element_parents[] = 'format';
  $format_element = array(
    '#parents' => $format_element_parents,
  );
  form_set_value($format_element, $original_value['format'], $form_state);
}

/**
 * Obtains entity URI and label of an entity, identified by UUID.
 *
 * @param string $uuid
 *   UUID of an entity of $entity_type.
 * @param string $entity_type
 *   Entity type string.
 *
 * @return array
 *   An associative array containing:
 *   - uri: The entity URI of the identified entity.
 *   - label: The entity label of the identified entity.
 */
function uuid_notes_uuid_get_uri_label($uuid, $entity_type) {
  $entities = entity_uuid_load($entity_type, array($uuid));
  $entity = array_shift($entities);
  $uri = entity_uri($entity_type, $entity);
  $label = entity_label($entity_type, $entity);

  return array(
    'uri' => $uri,
    'label' => $label,
  );
}

/**
 * Autocomplete callback: Returns UUIDs of entities with matching labels.
 *
 * @param string $token
 *   Token string ensuring only Drupal can get to this callback (security).
 * @param string $entity_type
 *   Entity type for which the matching should be done.
 * @param string $string
 *   The input string which should be matched against entity labels.
 *
 * @return string|null
 *   JSON string containing matches for Drupal's 'autocomplete.js'.
 */
function uuid_notes_autocomplete($token, $entity_type, $string = '') {
  global $user;
  if ($user->uid && drupal_valid_token($token, 'uuid_notes_autocomplete', TRUE)) {
    $callbacks = module_invoke_all("{$entity_type}_uuid_autocomplete_query_callbacks");
    $results = array();
    foreach ($callbacks as $callback) {
      if (function_exists($callback)) {
        $results += call_user_func($callback, $string);
      }
    }

    $matches = array();
    if (!empty($string) && !empty($results)) {
      foreach ($results as $result) {
        $matches[$result['uuid']] = "{$result['label']} ({$result['uuid']})";
      }
    }

    drupal_json_output($matches);
  }
  else {
    drupal_access_denied();
    return NULL;
  }
}
