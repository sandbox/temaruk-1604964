<?php
/**
 * @file
 * Contains general UI functionality.
 */


/**
 * Display the entity.
 *
 * @todo Determine if deprecated.
 */
function documentation_entity_view($entity, $view_mode = 'default') {

  $entity->content = array(
    '#view_mode' => $view_mode,
  );

  field_attach_prepare_view(ENTITY_NAME_DOCUMENTATION_ENTITY, array($entity->htid => $entity), $view_mode);
  entity_prepare_view(ENTITY_NAME_DOCUMENTATION_ENTITY, array($entity->htid => $entity));
  $entity->content += field_attach_view(ENTITY_NAME_DOCUMENTATION_ENTITY, $entity, $view_mode);

  $entity_type = ENTITY_NAME_DOCUMENTATION_ENTITY;
  drupal_alter(array(
    ENTITY_NAME_DOCUMENTATION_ENTITY . '_view',
    'entity_view',
  ), $entity->content, $entity_type);

  return $entity->content;
}

/**
 * Display the entity.
 */
function documentation_entity_page_view($documentation_entity, $view_mode = 'full') {
  $controller = entity_get_controller(ENTITY_NAME_DOCUMENTATION_ENTITY);
  $content = $controller->view(array($documentation_entity->htid => $documentation_entity), $view_mode);
  return $content;
}
