<?php
/**
 * @file
 * Contains DocumentationEntity and DocumentationEntityEntityController.
 */

/**
 * Represents a Documentation Entity.
 */
class DocumentationEntity extends Entity {

  /**
   * The primary identifier of this Documentation Entity.
   *
   * @var int
   */
  public $htid;

  /**
   * The identifier of the current revision of this Documentation Entity.
   *
   * @var int
   */
  public $htvid;

  /**
   * The Universally Unique Identifier of this Documentation Entity.
   *
   * @var string
   */
  public $uuid;

  /**
   * The Universally Unique Identifier
   * of the current revision of this Documentation Entity.
   *
   * @var string
   */
  public $vuuid;

  /**
   * The user id of the author of this Documentation Entity.
   *
   * @var int
   */
  public $uid;

  /**
   * The type (bundle) of this Documentation Entity.
   *
   * @var string
   */
  public $type;

  /**
   * The Unix timestamp of when this Documentation Entity was created.
   *
   * @var int
   */
  public $created;

  /**
   * The Unix timestamp of when this Documentation Entity was most recently
   * saved.
   *
   * @var int
   */
  public $changed;

  /**
   * Overrides Entity::__construct().
   *
   * @see Entity::__construct()
   */
  public function __construct($values = array()) {
    parent::__construct($values, ENTITY_NAME_DOCUMENTATION_ENTITY);
  }

  /**
   * Overrides Entity::defaultUri().
   *
   * @see Entity::defaultUri()
   */
  protected function defaultUri() {
    return array('path' => 'documentation_entity/' . $this->htid);
  }
}

/**
 * Defines common interface for DocumentationEntityController.
 */
interface DocumentationEntityControllerInterface extends DrupalEntityControllerInterface {

  /**
   * Overrides EntityAPIControllerInterface::save().
   *
   * @abstract
   *
   * @see EntityAPIControllerInterface::save()
   */
  public function save($entity, DatabaseTransaction $transaction = NULL);

  /**
   * Override EntityAPIControllerInterface::delete().
   *
   * @abstract
   *
   * @see EntityAPIControllerInterface::delete()
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL);
}

/**
 * Controller for a Documentation Entity.
 */
class DocumentationEntityEntityController extends EntityAPIController implements DocumentationEntityControllerInterface {

  /**
   * Overrides EntityAPIController::__construct().
   *
   * @see EntityAPIController::__construct()
   */
  public function __construct($entity_type) {
    parent::__construct($entity_type);
  }

  /**
   * Overrides EntityAPIController::save().
   *
   * As the Entity API does not have a stable release with revision support yet,
   * the code was borrowed from the Commerce module.
   *
   * @see EntityAPIController::save()
   * @see DrupalCommerceEntityController::save()
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    $transaction = isset($transaction) ? $transaction : db_transaction();
    try {
      // Load the stored entity, if any.
      if (!empty($entity->{$this->idKey}) && !isset($entity->original)) {
        // In order to properly work in case of name changes, load the original
        // entity using the id key if it is available.
        $entity->original = entity_load_unchanged($this->entityType, $entity->{$this->idKey});
      }

      $this->invoke('presave', $entity);

      if (!empty($this->revisionKey) && empty($entity->is_new) && !empty($entity->revision) && !empty($entity->{$this->revisionKey})) {
        $entity->old_revision_id = $entity->{$this->revisionKey};
        unset($entity->{$this->revisionKey});
      }

      if (!empty($entity->{$this->idKey}) && empty($entity->is_new)) {
        $op = 'update';

        $return = drupal_write_record($this->entityInfo['base table'], $entity, $this->idKey);

        if (!empty($this->revisionKey)) {
          if (!empty($entity->revision)) {
            drupal_write_record($this->entityInfo['revision table'], $entity);
            $update_base_table = TRUE;
          }
          else {
            drupal_write_record($this->entityInfo['revision table'], $entity, $this->revisionKey);
          }
        }

        $this->resetCache(array($entity->{$this->idKey}));
      }
      else {
        $op = 'insert';

        $return = drupal_write_record($this->entityInfo['base table'], $entity);
        if (!empty($this->revisionKey)) {
          drupal_write_record($this->entityInfo['revision table'], $entity);
          $update_base_table = TRUE;
        }
      }

      if (!empty($update_base_table)) {
        // Go back to the base table and update the pointer to the revision ID.
        db_update($this->entityInfo['base table'])
          ->fields(array($this->revisionKey => $entity->{$this->revisionKey}))
          ->condition($this->idKey, $entity->{$this->idKey})
          ->execute();
      }

      $this->invoke($op, $entity);

      // Ignore slave server temporarily.
      db_ignore_slave();
      unset($entity->is_new);
      unset($entity->original);
      unset($entity->revision);

      return $return;
    } catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception($this->entityType, $e);
      throw $e;
    }
  }

  /**
   * Overrides EntityAPIController::delete().
   *
   * As the Entity API does not have a stable release with revision support yet,
   * the code was borrowed from the Commerce module.
   *
   * @see EntityAPIController::delete()
   * @see DrupalCommerceEntityController::delete()
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    $entities = $ids ? $this->load($ids) : FALSE;
    if (!$entities) {
      // Do nothing, in case invalid or no ids have been passed.
      return;
    }

    if (!isset($transaction)) {
      $transaction = db_transaction();
      $started_transaction = TRUE;
    }

    try {
      db_delete($this->entityInfo['base table'])
        ->condition($this->idKey, array_keys($entities), 'IN')
        ->execute();
      if (!empty($this->revisionKey)) {
        db_delete($this->entityInfo['revision table'])
          ->condition($this->idKey, array_keys($entities), 'IN')
          ->execute();
      }
      // Reset the cache as soon as the changes have been applied.
      $this->resetCache($ids);

      foreach ($entities as $id => $entity) {
        $this->invoke('delete', $entity);
      }
      // Ignore slave server temporarily.
      db_ignore_slave();

      return TRUE;
    }
    catch (Exception $e) {
      if (!empty($started_transaction)) {
        $transaction->rollback();
        watchdog_exception($this->entityType, $e);
      }
      throw $e;
    }
  }

  /**
   * Overrides EntityAPIController::buildContent().
   *
   * Adds entity properties to the display.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper(ENTITY_NAME_DOCUMENTATION_ENTITY, $entity);

    $content['uuid'] = array(
      '#type' => 'item',
      '#title' => t('UUID:'),
      '#markup' => $wrapper->uuid->value(),
    );

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}
