<?php
/**
 * @file
 * Documentation Entity editing UI.
 */

/**
 * Documentation Entity entity UI controller.
 */
class DocumentationEntityUIController extends EntityDefaultUIController {

  /**
   * Overrides EntityDefaultUIController::hook_menu().
   *
   * @see EntityDefaultUIController::hook_menu()
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $module_path = drupal_get_path('module', $this->entityInfo['module']);
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    // Change the overview menu path type.
    $items[$this->path]['type'] = MENU_LOCAL_TASK;

    // General Documentation Entity add menu path.
    $items["{$this->path}/add"] = array(
      'title' => 'Add Documentation Entity',
      'page callback' => 'documentation_entity_add_page',
      'access arguments' => array(PERMISSION_CREATE_DOCUMENTATION_ENTITY),
      'file' => 'documentation_entity.admin.inc',
      'file path' => "{$module_path}/includes",
      'menu_name' => 'navigation',
    );

    // Bundle specific Documentation Entity add menu path.
    foreach ($this->entityInfo['bundles'] as $type => $info) {
      $items["{$this->path}/add/{$type}"] = array(
        'title' => "Add {$info['label']}",
        'page callback' => 'documentation_entity_add',
        'page arguments' => array($type),
        'access arguments' => array(PERMISSION_CREATE_DOCUMENTATION_ENTITY),
        'file' => 'documentation_entity.admin.inc',
        'file path' => "{$module_path}/includes",
        'description' => isset($info['description']) ? $info['description'] : '',
      );
    }

    return $items;
  }

  /**
   * Overrides EntityDefaultUIController::overviewTable().
   *
   * Overridden to add additional columns to table:
   *   - Type
   *   - Author
   *   - Updated
   *
   * @see EntityDefaultUIController::overviewTable()
   */
  public function overviewTable($conditions = array()) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $this->entityType);

    // Add all conditions to query.
    foreach ($conditions as $key => $value) {
      $query->propertyCondition($key, $value);
    }

    if ($this->overviewPagerLimit) {
      $query->pager($this->overviewPagerLimit);
    }

    $results = $query->execute();

    $ids = isset($results[$this->entityType]) ? array_keys($results[$this->entityType]) : array();
    $entities = $ids ? entity_load($this->entityType, $ids) : array();
    ksort($entities);

    $rows = array();
    foreach ($entities as $entity) {
      $additional_columns = array();
      $additional_columns[] = check_plain($entity->type);
      $additional_columns[] = theme('username', array('account' => user_load($entity->uid)));
      $additional_columns[] = format_date($entity->changed, 'short');
      $rows[] = $this->overviewTableRow($conditions, entity_id($this->entityType, $entity), $entity, $additional_columns);
    }

    $additional_headers = array(
      'type' => t('Type'),
      'author' => t('Author'),
      'changed' => t('Updated'),
    );
    $render = array(
      '#theme' => 'table',
      '#header' => $this->overviewTableHeaders($conditions, $rows, $additional_headers),
      '#rows' => $rows,
      '#empty' => t('No Documentation Entities available.'),
    );
    return $render;
  }
}

/**
 * Page callback: Displays administration page of Documentation Entity types.
 *
 * @param string|null $type
 *   Documentation Entity type for which the administration page should be
 *   displayed. Defaults to NULL.
 *
 * @return string
 *   If $type is NULL, administration page of Documentation Entity $type.
 *   Otherwise, list of links to administration pages of Documentation Entity
 *   types.
 *
 * @see documentation_entity_menu()
 */
function documentation_entity_types_admin_page($type = NULL) {
  if ($type === NULL) {
    $types = documentation_entity_get_types();

    $output = '<dl class="documentation-entity-type-list">';
    foreach ($types as $type => $info) {
      $description = isset($info['description']) ? filter_xss_admin($info['description']) : '';
      $output .= '<dt>' . l($info['label'], $info['admin']['real path']) . '</dt>';
      $output .= '<dd>' . $description . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    $output = '<p>' . t('Settings page for %type.', array('%type' => $type)) . '</p>';
  }

  return $output;
}

/**
 * Page callback: General add page with links to Documentation Entity types.
 *
 * @return string
 *   HTML for list of bundle specific Documentation Entity add links.
 *
 * @see theme_documentation_entity_add_list()
 */
function documentation_entity_add_page() {
  $item = menu_get_item();
  $content = system_admin_menu_block($item);
  // Bypass the documentation_entity/add listing if only one content type is
  // available.
  if (count($content) == 1) {
    $item = array_shift($content);
    drupal_goto($item['href']);
  }
  return theme('documentation_entity_add_list', array('content' => $content));
}

/**
 * Returns list of available Documentation Entity types for creation.
 *
 * @param array $variables
 *   An associative array containing:
 *   - content: An array of Documentation Entity types.
 *
 * @ingroup themeable
 *
 * @return string
 *   HTML for Documentation Entity add list.
 */
function theme_documentation_entity_add_list($variables) {
  $content = $variables['content'];

  $output = '<dl class="documentation-entity-type-list">';
  foreach ($content as $item) {
    $output .= '<dt>' . l($item['title'], $item['href'], $item['localized_options']) . '</dt>';
    $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
  }
  $output .= '</dl>';

  return $output;
}

/**
 * Menu callback: Bundle specific Documentation Entity add page.
 */
function documentation_entity_add($type) {
  $values = array(
    'type' => $type,
  );
  $entity = entity_get_controller(ENTITY_NAME_DOCUMENTATION_ENTITY)->create($values);
  return entity_ui_get_form(ENTITY_NAME_DOCUMENTATION_ENTITY, $entity, 'add');
}

/**
 * Form builder: Documentation Entity edit/create form.
 */
function documentation_entity_form($form, &$form_state, $entity, $op, $entity_type) {
  field_attach_form(ENTITY_NAME_DOCUMENTATION_ENTITY, $entity, $form, $form_state);

  $documentation_entity_types = documentation_entity_get_types();
  switch ($op) {
    case 'add':
      drupal_set_title(t('Add @documentation_entity_type', array('@documentation_entity_type' => $documentation_entity_types[$entity->type]['label'])));
  }

  $form['revision_settings'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-revision')),
    '#weight' => 300,
  );

  $form['revision_settings']['revision'] = array(
    '#title' => 'Create new revision',
    '#type' => 'checkbox',
    '#default_value' => 1,
  );

  $form['revision_settings']['log'] = array(
    '#title' => 'Revision log message',
    '#type' => 'textarea',
    '#states' => array(
      'visible' => array(
        ':input[name="revision"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Validation handler for documentation_entity_form().
 */
function documentation_entity_form_validate(&$form, &$form_state) {
  $documentation_entity = $form_state[ENTITY_NAME_DOCUMENTATION_ENTITY];
  field_attach_form_validate(ENTITY_NAME_DOCUMENTATION_ENTITY, $documentation_entity, $form, $form_state);
}

/**
 * Submit handler for documentation_entity_form().
 *
 * @see DocumentationEntityEntityController::save()
 */
function documentation_entity_form_submit(&$form, &$form_state) {
  $documentation_entity = entity_ui_controller(ENTITY_NAME_DOCUMENTATION_ENTITY)->entityFormSubmitBuildEntity($form, $form_state);

  // Add in created and changed times.
  if ($documentation_entity->is_new = isset($documentation_entity->is_new) ? $documentation_entity->is_new : 0) {
    $documentation_entity->created = REQUEST_TIME;
  }

  $documentation_entity->changed = REQUEST_TIME;
  $documentation_entity->uid = $GLOBALS['user']->uid;

  // Enable revision creation on the entity,
  // for later use in DocumentationEntityEntityController::save().
  if ($form_state['values']['revision'] || !empty($form_state['values']['log'])) {
    $documentation_entity->revision = TRUE;
    $documentation_entity->log = $form_state['values']['log'];
  }

  $documentation_entity->save();
  $form_state['redirect'] = 'admin/content/documentation_entity';
}
