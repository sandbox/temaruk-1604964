<?php
/**
 * @file
 * Contains module specific custom fields, and field definitions in code.
 */

/**
 * Add long text field to a Documentation Entity type.
 *
 * @param string $field_name
 *   Machine name of the field.
 * @param string $documentation_entity_type
 *   Documentation Entity type.
 * @param string $description
 *   Field instance description.
 * @param string $label
 *   The label for the short description instance.
 * @param array $additional_config
 *   Additional field and instance configuration settings.
 *   - cardinality
 *   - weight
 *   - default display label
 *   - default display type
 *
 * @return array
 *   Long text field instance.
 */
function documentation_entity_add_long_text_field($field_name, $documentation_entity_type, $description, $label, $additional_config = array()) {
  $field = field_info_field($field_name);
  $instance = field_info_instance(ENTITY_NAME_DOCUMENTATION_ENTITY, $field_name, $documentation_entity_type);

  if (empty($field)) {
    $field = array(
      'field_name' => $field_name,
      'type' => 'text_long',
      'entity_types' => array(ENTITY_NAME_DOCUMENTATION_ENTITY),
      'settings' => array(
        'no_ui' => TRUE,
      ),
      'locked' => TRUE,
      'translatable' => TRUE,
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
    );

    if (isset($additional_config['cardinality'])) {
      $field['cardinality'] = $additional_config['cardinality'];
    }

    $field = field_create_field($field);
  }

  if (empty($instance)) {
    $instance = array(
      'field_name' => $field_name,
      'entity_type' => ENTITY_NAME_DOCUMENTATION_ENTITY,
      'bundle' => $documentation_entity_type,
      'label' => $label,
      'description' => $description,
      'widget' => array(
        'type' => 'text_textarea',
      ),
      'settings' => array(
        'no_ui' => TRUE,
        'text_processing' => '1',
      ),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'type' => 'text_default',
        ),
      ),
    );

    if (isset($additional_config['weight']) || is_numeric($additional_config['weight'])) {
      $instance['widget']['weight'] = $additional_config['weight'];
      $instance['display']['default']['weight'] = $additional_config['weight'];
    }
    if (isset($additional_config['display_label'])) {
      $instance['display']['default']['label'] = $additional_config['display_label'];
    }
    if (isset($additional_config['display_type'])) {
      $instance['display']['default']['type'] = $additional_config['display_type'];
    }

    $instance = field_create_instance($instance);
  }

  return $instance;
}

/**
 * Add short text field to a Documentation Entity type.
 *
 * @param string $field_name
 *   Machine name of the field.
 * @param string $documentation_entity_type
 *   Documentation Entity type.
 * @param string $description
 *   Field instance description.
 * @param string $label
 *   The label for the components instance.
 * @param array $additional_config
 *   Additional field and instance configuration settings.
 *   - cardinality
 *   - weight
 *   - default display label
 *   - default display type
 *
 * @return array
 *   Short text field instance.
 */
function documentation_entity_add_short_text_field($field_name, $documentation_entity_type, $description, $label, $additional_config = array()) {
  $field = field_info_field($field_name);
  $instance = field_info_instance(ENTITY_NAME_DOCUMENTATION_ENTITY, $field_name, $documentation_entity_type);

  if (empty($field)) {
    $field = array(
      'field_name' => $field_name,
      'type' => 'text',
      'entity_types' => array(ENTITY_NAME_DOCUMENTATION_ENTITY),
      'settings' => array(
        'no_ui' => TRUE,
      ),
      'locked' => TRUE,
      'translatable' => TRUE,
    );

    if (isset($additional_config['cardinality'])) {
      $field['cardinality'] = $additional_config['cardinality'];
    }

    $field = field_create_field($field);
  }

  if (empty($instance)) {
    $instance = array(
      'field_name' => $field_name,
      'entity_type' => ENTITY_NAME_DOCUMENTATION_ENTITY,
      'bundle' => $documentation_entity_type,
      'label' => $label,
      'description' => $description,
      'widget' => array(
        'type' => 'text_textfield',
      ),
      'settings' => array(
        'no_ui' => TRUE,
      ),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'type' => 'text_default',
        ),
      ),
    );

    if (isset($additional_config['weight']) || is_numeric($additional_config['weight'])) {
      $instance['widget']['weight'] = $additional_config['weight'];
      $instance['display']['default']['weight'] = $additional_config['weight'];
    }
    if (isset($additional_config['display_label'])) {
      $instance['display']['default']['label'] = $additional_config['display_label'];
    }
    if (isset($additional_config['display_type'])) {
      $instance['display']['default']['type'] = $additional_config['display_type'];
    }

    $instance = field_create_instance($instance);
  }

  return $instance;
}

/**
 * Add a related topic field to a Documentation Entity type.
 *
 * @param string $field_name
 *   Machine name of the field.
 * @param string $documentation_entity_type
 *   Documentation Entity type.
 * @param string $description
 *   Field instance description.
 * @param string $label
 *   The label for the short description instance.
 * @param array $additional_config
 *   Additional field and instance configuration settings.
 *   - cardinality
 *   - weight
 *   - default display label
 *   - default display type
 *
 * @return array
 *   Related topic field instance.
 */
function documentation_entity_add_related_topics_field($field_name, $documentation_entity_type, $description, $label, $additional_config = array()) {
  $field = field_info_field($field_name);
  $instance = field_info_instance(ENTITY_NAME_DOCUMENTATION_ENTITY, $field_name, $documentation_entity_type);

  if (empty($field)) {
    $field = array(
      'field_name' => $field_name,
      'type' => 'uuid_notes',
      'entity_types' => array(ENTITY_NAME_DOCUMENTATION_ENTITY),
      'settings' => array(
        'no_ui' => TRUE,
      ),
      'locked' => TRUE,
      'translatable' => TRUE,
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
    );

    if (isset($additional_config['cardinality'])) {
      $field['cardinality'] = $additional_config['cardinality'];
    }

    $field = field_create_field($field);
  }

  if (empty($instance)) {
    $instance = array(
      'field_name' => $field_name,
      'entity_type' => ENTITY_NAME_DOCUMENTATION_ENTITY,
      'bundle' => $documentation_entity_type,
      'label' => $label,
      'description' => $description,
      'settings' => array(
        'no_ui' => TRUE,
        'uuid_entity_type' => ENTITY_NAME_DOCUMENTATION_ENTITY,
      ),
    );

    if (isset($additional_config['uuid_required'])) {
      $instance['settings']['uuid_required'] = $additional_config['uuid_required'];
    }
    if (isset($additional_config['notes_required'])) {
      $instance['settings']['notes_required'] = $additional_config['notes_required'];
    }
    if (isset($additional_config['weight']) || is_numeric($additional_config['weight'])) {
      $instance['widget']['weight'] = $additional_config['weight'];
      $instance['display']['default']['weight'] = $additional_config['weight'];
    }
    if (isset($additional_config['display_label'])) {
      $instance['display']['default']['label'] = $additional_config['display_label'];
    }
    if (isset($additional_config['display_type'])) {
      $instance['display']['default']['type'] = $additional_config['display_type'];
    }

    $instance = field_create_instance($instance);
  }

  return $instance;
}
